package scheduler;

import partition.*;
import scheduler.*;

import java.util.*;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;

import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.InputStreamReader;

import java.io.IOException;


public class SchedulerTest {

	public static void main(String args[]){
		PartitionList files = PartitionList.newFiles(args[0]); // read the file partitions
		PartitionList keys = PartitionList.newKeys(files); // get the key
		PartitionList filters = PartitionList.newFilters(keys, args[1]); // filter the key
		PartitionList count = PartitionList.newCounters(filters); // count

		Scheduler scheduler = new Scheduler();
		scheduler.run(files);
		scheduler.run(keys);
		scheduler.run(filters);
		scheduler.run(count);
		count.print();
	}  

}
