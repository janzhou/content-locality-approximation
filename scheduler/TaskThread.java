package scheduler;

import partition.*;

public class TaskThread extends Thread {
    public PartitionList list;

    public TaskThread(){
	list = new PartitionList(null);
    }

    public void Add(Partition p){
	list.Add(p);
    }

    public void run() {
	Partition p;
	while((p = list.Next()) != null) {
			p.construct();
		}
    }
}

