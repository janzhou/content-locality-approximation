package scheduler;

import partition.*;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class TaskStage {
	public List<TaskThread> list;

	public TaskStage(){
		list = new ArrayList<TaskThread>();
	}

	public void Add(TaskThread thread){
		this.list.add(thread);
	}

	public void run() {
		for(TaskThread thread : list) {
			thread.start();
		}

		for(TaskThread thread : list) {
			try{
				thread.join();
			}catch(InterruptedException e){
				e.printStackTrace();
			}
		}
	}
}

