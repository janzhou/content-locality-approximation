package scheduler;

import partition.*;
import metadata.*;

import java.util.*;

public class Scheduler {
	public int cores;
	private MetaData index;

	public Scheduler(){
		index = new MetaData("data/metadata", 171);
		cores = Runtime.getRuntime().availableProcessors();
	}

	public Scheduler(boolean nometa){
		cores = Runtime.getRuntime().availableProcessors();
	}

	private TaskStage defaultSplit(PartitionList list, int num_threads){
		int count = list.Size()/num_threads;
		TaskThread thread;
		TaskStage stage = new TaskStage();
		for(int i = 0; i < num_threads; i++){
			thread = new TaskThread();
			for(int j = 0; j < count; j++){
				thread.Add(list.Next());
			}
			stage.Add(thread);
		}

		thread = new TaskThread();
			Partition p;
while((p = list.Next()) != null){
				thread.Add(p);
			}
			stage.Add(thread);

		return stage;
	}

	private TaskStage weightedSplit(PartitionList list, String key, int num_threads){
		List<Partition> pl = list.GetList();
		int n = 0;
		for(Partition p : pl){
			p.weight = index.Query(n++, key);
		}
		Collections.sort(pl);

		List<TaskThread> thread = new ArrayList<TaskThread>();
		List<Integer> weight = new ArrayList<Integer>();
		TaskStage stage = new TaskStage();
		for(int i = 0; i < num_threads; i++){
			thread.add(new TaskThread());
			weight.add(0);
		}

		Iterator<Partition> intItr = pl.iterator();
		while(intItr.hasNext()){
			// get least
			int least = 0;
			for(int i = 0; i < num_threads; i++){
				if(weight.get(i) < weight.get(least)){
					least = i;
				}
			}

			Partition p = intItr.next();
			thread.get(least).Add(p);
			weight.set(least, weight.get(least) + p.weight);
		}

		for(int i = 0; i < num_threads; i++){
			//System.out.println(""+weight.get(i));
			stage.Add(thread.get(i));
		}
		return stage;
	}

	private TaskStage approximateSplit(PartitionList list, String key, int num_threads, double degree, int sort){
		List<Partition> pl = list.GetList();
		int n = 0;
		int total_weight = 0;
		for(Partition p : pl){
			p.weight = index.Query(n++, key);
			total_weight += p.weight;
		}
		
		if(sort != 0) {
			Collections.reverse(pl);
		}

		if(sort == 2) Collections.sort(pl);

		total_weight = (int)((double)total_weight * degree);

		List<TaskThread> thread = new ArrayList<TaskThread>();
		List<Integer> weight = new ArrayList<Integer>();
		TaskStage stage = new TaskStage();
		for(int i = 0; i < num_threads; i++){
			thread.add(new TaskThread());
			weight.add(0);
		}

		Iterator<Partition> intItr = pl.iterator();
		while(intItr.hasNext()){
			// get least
			int least = 0;
			for(int i = 0; i < num_threads; i++){
				if(weight.get(i) < weight.get(least)){
					least = i;
				}
			}

			Partition p = intItr.next();
			thread.get(least).Add(p);
			weight.set(least, weight.get(least) + p.weight);
			total_weight -= p.weight;
			if(total_weight <= 0) break;
		}

		for(int i = 0; i < num_threads; i++){
			//System.out.println(""+weight.get(i));
			stage.Add(thread.get(i));
		}
		return stage;
	}

	public void run(PartitionList list) {
		TaskStage stage = defaultSplit(list, cores);
		stage.run();
	}

	public void run(PartitionList list, int num_threads) {
		TaskStage stage = defaultSplit(list, num_threads);
		stage.run();
	}

	public void run(PartitionList list, String key) {
		TaskStage stage = weightedSplit(list, key, cores);
		stage.run();
	}

	public void run(PartitionList list, String key, int num_threads) {
		TaskStage stage = weightedSplit(list, key, num_threads);
		stage.run();
	}

	public void run(PartitionList list, String key, double degree, int sort) {
		TaskStage stage = approximateSplit(list, key, cores, degree, sort);
		stage.run();
	}
}

