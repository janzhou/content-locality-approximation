package partition;

import partition.*;

import java.util.ArrayList;
import java.util.List;

public class PartitionKey extends Partition {
	private List<String> keys = new ArrayList<String>();

	public PartitionKey(Partition p){
		super(p);
		keys = new ArrayList<String>();
	}

	public void compute(Object obj) {
		String line = (String) obj;
		String key = line.split(" ")[1];
		keys.add(key);
	}

	public void iterate(PartitionCallback callback){
		for (String key: keys) {
			callback.compute(key);
		}
	}
}
