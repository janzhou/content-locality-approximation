package partition;

import partition.*;

import java.util.*;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;

import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.InputStreamReader;

import java.io.IOException;

public class PartitionFile extends Partition {
	private String file;

	BufferedReader reader;

	public PartitionFile(String file) {
		super(null);
		this.file = file;
	}

	public void construct(){
		try{
			File file = new File(this.file);
			FileReader fr = new FileReader(file);
			reader = new BufferedReader(fr);
			String line;
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String readline(){
		String  line;
		try{
			if((line = reader.readLine()) != null) {
				return line;
			} else {
				construct();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void compute(Object obj) {
		return;
	}

	public void iterate(PartitionCallback callback){
		String line;
		while( ( line = readline()) != null ) {
			callback.compute(line);
		}
	}
}
