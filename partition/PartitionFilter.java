package partition;

import partition.*;

import java.util.ArrayList;
import java.util.List;

public class PartitionFilter extends Partition {
	private List<String> keys = new ArrayList<String>();
	private String filter;

	public PartitionFilter(Partition p, String filter){
		super(p);
		keys = new ArrayList<String>();
		this.filter = filter;
	}

	public void compute(Object obj) {
		String key = (String) obj;
		if( key.equals(filter) ) {
			keys.add(key);
		}
	}

	public void iterate(PartitionCallback callback){
		for (String key: keys) {
			callback.compute(key);
		}
	}
}
