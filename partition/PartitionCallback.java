package partition;

public interface PartitionCallback {
    public void compute(Object item);
}
