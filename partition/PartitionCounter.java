package partition;

import partition.*;

import java.util.ArrayList;
import java.util.List;

public class PartitionCounter extends Partition {
	public int count;

	public PartitionCounter(Partition p){
		super(p);
		count = 0;
	}

	public void compute(Object obj) {
		count++;
		/*try {
    			Thread.sleep(1);                 //1000 milliseconds is one second.
		} catch(InterruptedException ex) {
			Thread.currentThread().interrupt();
		}
		long delay = 100;
		long start = System.nanoTime();
		while(start + delay >= System.nanoTime());*/
	}

	public void iterate(PartitionCallback callback){
		callback.compute(count);
	}
}
