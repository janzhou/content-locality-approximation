package partition;

import partition.PartitionCallback;

import java.util.*;

public abstract class Partition implements PartitionCallback, Comparable<Partition> {
    private int id;
    public Partition parrent;
    private static int _id_ = 0;

    public int weight;

    public Partition(Partition p) {
        id = _id_++;
	parrent = p;
    }

    public int who() {
        return id;
    }

    // data construction function
    public void construct(){
	parrent.iterate(this);
    }

    // callback function run by parrent
    public abstract void compute(Object obj);

    // run callback function on each item
    public abstract void iterate(PartitionCallback callback);

    public void print(){
        this.iterate(new PartitionCallback(){
		public void compute(Object obj) {
			String s = "" + obj;
			System.out.println(s);
		}
	});
    }
	public int compareTo(Partition compare) {
 		//ascending order
		//return this.weight - compare.weight;
 		//descending order
		return compare.weight - this.weight;
 
	}
}
