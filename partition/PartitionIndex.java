package partition;

import java.util.*;
import java.io.*;

public class PartitionIndex extends Partition {
	private HashMap<String, Integer> index;

	public PartitionIndex(Partition p){
		super(p);
		index = new HashMap<>();
	}

	public int Query(String key){
		if(index.containsKey(key)) {
			return index.get(key);
		}
		return 0;
	}

	public void compute(Object obj) {
		String key = (String) obj;
		if(!index.containsKey(key)){
			index.put(key,1);
		} else { 
			int val = index.get(key);
			index.put(key, val+1);
		}
	}

	public void iterate(PartitionCallback callback){
		for (Map.Entry<String, Integer> entry : index.entrySet()) {
			String key = entry.getKey();
			Integer value = entry.getValue();
			callback.compute(entry);
		}
	}

	public void toFile(String filename) {
		try{
			File file = new File(filename);
			FileOutputStream f = new FileOutputStream(file);
			ObjectOutputStream s = new ObjectOutputStream(f);
			s.writeObject(index);
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void fromFile(String filename) {
		try{
			File file = new File(filename);
			FileInputStream f = new FileInputStream(file);
			ObjectInputStream s = new ObjectInputStream(f);
			try{
				index = (HashMap<String, Integer>) s.readObject();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}

