package partition;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import java.util.*;
import java.io.File;
import java.io.FileReader;
import java.io.BufferedReader;

import java.io.FileWriter;
import java.io.BufferedWriter;
import java.io.InputStreamReader;

import java.io.IOException;

public class PartitionList {
	public PartitionList parent;
	private List<Partition> list;
	private Iterator<Partition> intItr;

	public PartitionList(PartitionList p) {
		parent = p;
		intItr = null;
		list = new ArrayList<Partition>();
	}

	public Partition Next(){
		if( intItr == null ) {
			iterator();
		}
		if(intItr.hasNext())  {
			return intItr.next();
		} else {
			iterator();
			return null;
		}
	}

	public Partition Get(int id){
		return list.get(id);
	}

	public List<Partition> GetList(){
		return list;
	}

	public void Add(Partition p){
		list.add(p);
	}

	public int Size(){
		return list.size();
	}

	private void iterator(){
		intItr = list.iterator();
	}

	public void print(){
		Partition p;
		while((p = this.Next()) != null) {
			p.print();
		}
	}

	public static PartitionList newFiles(String partitions){
		PartitionList list = new PartitionList(null);

		try{
			File file = new File(partitions);
			FileReader fr = new FileReader(file);
			BufferedReader br = new BufferedReader(fr);
			String line;
			while((line = br.readLine()) != null){
				list.Add(new PartitionFile(line)); // read the file partition
			}
			br.close();
			fr.close();
		} catch (IOException e){
			e.printStackTrace();
		}
		return list;
	}

	public static PartitionList newKeys(PartitionList pl){
		PartitionList list = new PartitionList(pl);
		Partition p;
		while((p = pl.Next()) != null) {
			list.Add(new PartitionKey(p)); // get the key
		}
		return list;
	}

	public static PartitionList newFilters(PartitionList pl, String filter){
		PartitionList list = new PartitionList(pl);
		Partition p;
		while((p = pl.Next()) != null) {
			list.Add(new PartitionFilter(p, filter)); // filter the key
		}
		return list;
	}

	public static PartitionList newCounters(PartitionList pl){
		PartitionList list = new PartitionList(pl);
		Partition p;
		while((p = pl.Next()) != null) {
			list.Add(new PartitionCounter(p)); // count
		}
		return list;
	}

	public int totalCount(){
		PartitionList pl = this;
		PartitionCounter p;
		int count = 0;
		while((p = (PartitionCounter)pl.Next()) != null) {
			count += p.count; // count
		}
		return count;
	}
}
