package metadata;

import partition.*;
import scheduler.*;
import metadata.*;

public class MetaDataQuery {

	public static void main(String args[]){
		if (args.length != 3) {
			System.out.println("java metadata.MetaDataQuery metadata_dir number_of_metadata_files max_data_to_query");
			System.exit(1);
		}

		MetaData index = new MetaData(args[0], Integer.parseInt(args[1])); // index
		for( int i = 1; i <= Integer.parseInt(args[2]); i++) {
			String query = "" + i;
			System.out.println(query+" "+index.Query(query));
		}
	}  

}
