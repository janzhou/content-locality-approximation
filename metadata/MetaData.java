package metadata;

import partition.*;
import scheduler.*;

public class MetaData {
	private PartitionList index;

	public MetaData(PartitionList pl){
		PartitionList list = new PartitionList(pl);
		Partition p;
		while((p = pl.Next()) != null) {
			list.Add(new PartitionIndex(p)); // get the key
		}
		Scheduler scheduler = new Scheduler(true);
		scheduler.run(list);
		this.index = list;
	}

	public MetaData(String dir, int num){
		fromFile(dir, num);
	}

	public int Query(String key){
		PartitionIndex p;
		int count = 0;
		while((p = (PartitionIndex) index.Next()) != null) {
			count += p.Query(key);
		}
		return count;
	}

	public int Query(int partition, String key){
		PartitionIndex p = (PartitionIndex) index.Get(partition);
		return p.Query(key);
	}

	public void toFile(String dir){
		PartitionIndex p;
		int i = 0;
		while((p = (PartitionIndex) index.Next()) != null) {
			p.toFile(dir+"/"+i++);
		}
	}

	public void fromFile(String dir, int num){
		PartitionList list = new PartitionList(null);
		int i = 0;
		while(i < num) {
			PartitionIndex p = new PartitionIndex(null);
			p.fromFile(dir+"/"+i++);
			list.Add(p);
		}
		this.index = list;
	}
}
