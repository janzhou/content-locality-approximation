package metadata;

import partition.*;
import scheduler.*;
import metadata.*;

import java.util.*;
import java.io.*;

public class MetaDataRangeQuery {

	public static void main(String args[]){
		if (args.length != 3) {
			System.out.println("java metadata.MetaDataQuery metadata_dir number_of_metadata_files range_query.txt");
			System.exit(1);
		}

		int num_meta = Integer.parseInt(args[1]);
		MetaData index = new MetaData(args[0], num_meta); // index
for( int i = 1; i < num_meta; i++) {
		try{
			File file = new File(args[2]);
			FileReader fr = new FileReader(file);
			BufferedReader reader = new BufferedReader(fr);
			String key;
int count = 0;
			while((key = reader.readLine()) != null) {
					count += index.Query(i, key);
			}
			System.out.println(""+count);
		} catch (IOException e) {
			e.printStackTrace();
		}
}
	}  

}
