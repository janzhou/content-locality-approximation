package metadata;

import partition.*;
import scheduler.*;
import metadata.*;

public class MetaDataLocality {

	public static void main(String args[]){
		if (args.length != 3) {
			System.out.println("java metadata.MetaDataLocality metadata_dir number_of_metadata_files data_to_query");
			System.exit(1);
		}

		int npartitions = Integer.parseInt(args[1]);
		MetaData index = new MetaData(args[0], npartitions); // index

		String query = args[2];
		for( int i = 0; i < npartitions; i++) {
			System.out.println(""+(i+1)+" "+index.Query(i,query));
		}

	}  

}
