package metadata;

import partition.*;
import scheduler.*;
import metadata.*;

public class MetaDataConstruction {

	public static void main(String args[]){

		if (args.length != 2) {
			System.out.println("java metadata.MetaDataConstruction partitions.txt metadata_dir");
			System.exit(1);
		}

		PartitionList files = PartitionList.newFiles(args[0]); // read the file partitions
		PartitionList keys = PartitionList.newKeys(files); // get the key
		
		Scheduler scheduler = new Scheduler(true);
		scheduler.run(files);
		scheduler.run(keys);

		MetaData index = new MetaData(keys); // index
		index.toFile(args[1]);
	}  

}
