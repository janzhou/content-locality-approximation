package benchmark;

import partition.*;
import scheduler.*;
import metadata.*;

public class BenchmarkCount {

	public static void main(String args[]){
		if (args.length < 2) {
			System.out.println("java benchmark.BenchmarkCount partitions.txt max_data_to_query schedule");
			System.exit(1);
		}

		PartitionList files = PartitionList.newFiles(args[0]); // read the file partitions
		PartitionList keys = PartitionList.newKeys(files); // get the key

		Scheduler scheduler = new Scheduler();

//System.out.println(""+scheduler.cores);

		scheduler.run(files);
		scheduler.run(keys);
		for( int i = 1; i <= Integer.parseInt(args[1]); i++) {
			String query = "" + i;
			

			PartitionList filters = PartitionList.newFilters(keys, query);
			PartitionList count = PartitionList.newCounters(filters);

			scheduler.run(filters);

			long startTime = System.nanoTime();
			if(args.length == 3 && 1 == Integer.parseInt(args[2])){
				scheduler.run(count, query);
			} else scheduler.run(count);
			long endTime = System.nanoTime();

			int c = count.totalCount();
			long duration = (endTime - startTime); 
			
			System.out.println(query+" "+duration+" "+c);
		}
	}  

}
