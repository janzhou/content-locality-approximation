package benchmark;

import partition.*;
import scheduler.*;
import metadata.*;

public class Benchmark {

	public static void main(String args[]){
		if (args.length != 3) {
			System.out.println("java benchmark.Benchmark metadata_dir number_of_metadata_files max_data_to_query");
			System.exit(1);
		}

		MetaData index = new MetaData(args[0], Integer.parseInt(args[1])); // index
		for( int i = 1; i < Integer.parseInt(args[2]); i++) {
			String query = "" + i;
			long startTime = System.nanoTime();
			int count = index.Query(query);
			long endTime = System.nanoTime();

			long duration = (endTime - startTime); 
			
			System.out.println(query+" "+duration+" "+count);
		}
	}  

}
