package benchmark;

import partition.*;
import scheduler.*;
import metadata.*;

public class BenchmarkApproximate {

	public static void main(String args[]){
		if (args.length < 4) {
			System.out.println("java benchmark.BenchmarkApproximate partitions.txt max_data_to_query degree schedule sort");
			System.exit(1);
		}

		Scheduler scheduler = new Scheduler();

		double degree = Double.parseDouble(args[2]);

		for( int i = 1; i <= Integer.parseInt(args[1]); i++) {
			String query = "" + i;

			PartitionList files = PartitionList.newFiles(args[0]);
			PartitionList keys = PartitionList.newKeys(files);
			PartitionList filters = PartitionList.newFilters(keys, query);
			PartitionList count = PartitionList.newCounters(filters);

			long startTime = System.nanoTime();

			int sort = Integer.parseInt(args[3])
			scheduler.run(files, query, degree,sort);
			scheduler.run(keys, query, degree,sort);
			scheduler.run(filters, query, degree,sort);
			scheduler.run(count, query, degree,sort);

			long endTime = System.nanoTime();

			int c = count.totalCount();
			long duration = (endTime - startTime); 
			
			System.out.println(query+" "+duration+" "+c);
		}
	}  

}
